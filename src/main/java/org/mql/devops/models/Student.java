package org.mql.devops.models;




public class Student {
	
	
	private String cnie;
	private String firstname;
	private String lastname;
	private String course;

	public Student() {
	}

	public Student( String cnie,String firstname,String lastname, String course) {
		super();
		this.cnie = cnie;
		this.firstname = firstname;
		this.lastname = lastname;

		this.course = course;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getCnie() {
		return cnie;
	}

	public void setCnie(String cnie) {
		this.cnie = cnie;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}
	
	
}
