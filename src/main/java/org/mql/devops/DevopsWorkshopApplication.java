package org.mql.devops;

import java.util.List;

import org.mql.devops.models.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DevopsWorkshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevopsWorkshopApplication.class, args);
	}
	
	@Bean
	public List<Student> students(){
		return List.of(new Student("12345678901", "John", "Doe", "Computer Science"),
				new Student("23456789012", "Jane", "Smith", "Mathematics"),
				new Student("34567890123", "Alice", "Johnson", "Physics"),
				new Student("45678901234", "Bob", "Williams", "Chemistry"),
				new Student("56789012345", "Charlie", "Brown", "Biology"),
				new Student("67890123456", "David", "Jones", "History"),
				new Student("78901234567", "Eve", "Davis", "Literature"),
				new Student("89012345678", "Frank", "Miller", "Philosophy"),
				new Student("90123456789", "Grace", "Wilson", "Engineering"),
				new Student("01234567890", "Hannah", "Moore", "Art"),
				new Student("10987654321", "Ivy", "Taylor", "Economics"),
				new Student("19876543210", "Jack", "Anderson", "Political Science"),
				new Student("28765432109", "Kathy", "Thomas", "Sociology"),
				new Student("37654321098", "Leo", "Jackson", "Anthropology"),
				new Student("46543210987", "Mia", "White", "Psychology"),
				new Student("55432109876", "Nina", "Harris", "Geography"),
				new Student("64321098765", "Owen", "Martin", "Environmental Science"),
				new Student("73210987654", "Paula", "Thompson", "Law"),
				new Student("82109876543", "Quinn", "Garcia", "Medicine"),
				new Student("91098765432", "Rita", "Martinez", "Music"),
				new Student("01987654322", "Sam", "Robinson", "Architecture"),
				new Student("29876543211", "Tina", "Clark", "Education"),
				new Student("38765432110", "Uma", "Rodriguez", "Business Administration"),
				new Student("47654321099", "Vince", "Lewis", "Marketing"),
				new Student("56543210988", "Wendy", "Lee", "Journalism"),
				new Student("65432109877", "Xander", "Walker", "Graphic Design"),
				new Student("74321098766", "Yara", "Hall", "Nursing"),
				new Student("83210987655", "Zane", "Allen", "Statistics"),
				new Student("92109876544", "Amy", "Young", "International Relations"),
				new Student("01234567891", "Ben", "King", "Theology"));
	}

}
