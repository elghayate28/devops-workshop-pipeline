package org.mql.devops.dao;

import java.util.List;

import org.mql.devops.models.Student;

public interface StudentDao {
	public List<Student> selectAll();
	public boolean save(Student student);
	public Student deleteById(String cnie);
	public Student findById(String cnie);
}
