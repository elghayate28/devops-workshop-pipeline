package org.mql.devops.business;

import java.util.List;


import org.mql.devops.dao.StudentDao;
import org.mql.devops.models.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UniversityServiceDefault implements UniversityService{
	
	@Autowired
	private StudentDao studentDao;
	
	public UniversityServiceDefault() {
	}
	
	

	public UniversityServiceDefault(StudentDao studentDao) {
		this.studentDao = studentDao;
	}



	@Override
	public List<Student> students() {
		return studentDao.selectAll();
	}
	@Override
	 public void insertStudent(Student student) {
	       studentDao.save(student);
	    }

	@Override
	public Student deleteStudentByCnie(String cnie) {
		return studentDao.deleteById(cnie);
	}



	@Override
	public Student student(String cnie) {
		return studentDao.findById(cnie);
	}
	
}





